<?php
        class animal{
            public $name;
            public $legs = 4;
            public $cold_blooded = "no";

            public function __construct($name){
                $this->name = $name;
            }

            public function set_legs($legs){
                $this->legs = $legs;
                }
    
            public function get_legs(){
                    return $this->legs;
                }
        }
?>