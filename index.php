<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Class Activity 2</title>
    </head>
    <body>
    
        <?php
            require_once 'animal.php';
            require_once 'ape.php';
            require_once 'frog.php';

            $sheep = new animal("Shaun");

            echo "Name : " . $sheep->name . "<br>"; // "shaun"
            echo "Legs : " . $sheep->legs . "<br>"; // 4
            echo "Cold-Blooded : " . $sheep->cold_blooded . "<br>"; // "no"
            echo "<br>";

            $kodok = new frog("Buduk");
            $kodok->set_legs("4");
            echo "Name : " . $kodok->name . "<br>";
            echo "Legs : " . $kodok->legs . "<br>";
            echo "Cold-Blooded : " . $kodok->cold_blooded . "<br>";
            echo "Jump : ". $kodok->jump();
            echo "<br>";

            $sungokong = new ape("Kera Sakti");
            $sungokong->set_legs("2");
            echo "Name : " . $sungokong->name . "<br>";
            echo "Legs : " . $sungokong->legs . "<br>";
            echo "Cold-Blooded : " . $sungokong->cold_blooded . "<br>";
            echo "Yell : " . $sungokong->yell(); // "Auooo"
            echo "<br>";
        ?>

    </body>

</html>